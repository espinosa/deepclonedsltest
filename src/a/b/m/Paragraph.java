package a.b.m;

/** test dummy class */
public class Paragraph {
	private Integer number;
	private String text;
	
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
}
