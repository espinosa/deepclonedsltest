package a.b.m;

/** test dummy class */
public class Section {
	private String name; 
	private String description; 
	private Paragraph paragraph;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Paragraph getParagraph() {
		return paragraph;
	}
	public void setParagraph(Paragraph paragraph) {
		this.paragraph = paragraph;
	}
}
